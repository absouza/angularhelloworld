import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-botao',
  templateUrl: './botao.component.html',
  styleUrls: ['./botao.component.scss']
})
export class BotaoComponent implements OnInit {

contador = 0;

  constructor() { }

  ngOnInit(): void {
  }

  incrementaContador(){
    this.contador++;
  }

}
